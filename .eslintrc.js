module.exports = {
    extends: [
        'react-app',
        'react-app/jest',
        'plugin:prettier/recommended',
    ],
    rules: {
        
        'prettier/prettier': 1,
        'no-console':
            process.env.NODE_ENV === 'development' ? 0 : 1,
        'no-debugger':
            process.env.NODE_ENV === 'development' ? 0 : 1,
        'no-alert':
            process.env.NODE_ENV === 'development' ? 0 : 1,
        'no-var':
            process.env.NODE_ENV === 'development' ? 0 : 1,
        // args 选项有三个值：
        // after-used - 不检查最后一个使用的参数之前出现的未使用的位置参数，但是检查最后一个使用的参数之后的所有命名参数和所有位置参数。
        // all - 所有命名参数必须使用。
        // none - 不检查参数。
        'no-unused-vars': [
            1,
            { vars: 'all', args: 'none' },
        ],
    },
};
