import axios from "axios"



const httpTool=({timeout=10,failMessage=(msg)=>{
   alert(msg)
}})=>{
   const httpAxios=axios.create ({
    timeout:timeout*1000,
    baseURL:process.env.REACT_APP_BASE_url,
    retryDelay:1000,//设置等待时间
    retry :3//设置最大次数
   });

   httpAxios.interceptors.request.use(
      function (config) {
        
          return config;
      },
      function (error) {
        
           
         return Promise.reject(error);
      }
  );
  
  
  httpAxios.interceptors.response.use(
      function (response) {
        
          return response;
      },
      function (err) {
         var config = err.config;
          // 如果config不存在或未设置重试选项，请拒绝
          if(!config || !config.retry) return Promise.reject(err);
          // 设置变量跟踪重试次数
          config.__retryCount = config.__retryCount || 0;
          // 检查是否已经达到最大重试总次数
          if(config.__retryCount >= config.retry) {
           // 抛出错误信息

           failMessage("请求超时")

           return Promise.reject(err);
          }
          // 增加请求重试次数
          config.__retryCount += 1;
          // 创建新的异步请求
          var backoff = new Promise(function(resolve) {
           setTimeout(function() {
            resolve();
           }, config.retryDelay || 1);
          });
          // 返回axios信息，重新请求
          return backoff.then(function() {
           return httpAxios(config);
          });
         


         //  return Promise.reject(error);
      }
  );



   return httpAxios
}

export default httpTool


