import { useEffect, useState } from 'react';

// 获取img对象
const getImage = (imgurl) => {
    return new Promise((resolve, reject) => {
        const img = new Image();
        img.crossOrigin = 'Anonymous'; //前端需要做的,需要配合后端一起起，后端需要设置access-control-allow-origin *
        img.src = imgurl;
        img.onload = function () {
            resolve(img);
        };
        img.onerror = function (error) {
            // if(error)
            // {
            //   img.src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic.jj20.com%2Fup%2Fallimg%2F1111%2F0R11Q22326%2F1PR1122326-6.jpg&refer=http%3A%2F%2Fpic.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1666445676&t=484405a4388720eddabb3fce23fb01de"
            // }
            reject(error);
        };
    });
};
const drawCanvas = async (ctx, data, canvasDom) => {
    // 绘制填充背景
    ctx.fillStyle = '#fff';
    ctx.fillRect(0, 0, 350, 466);
    // 绘制标题
    ctx.font = '20px Georgia';
    ctx.fillStyle = '#000';
    ctx.fillText(data.title, 104, 30);

    // 绘制标题底部的线
    ctx.beginPath();
    ctx.strokeStyle = 'green'; // 蓝色路径
    ctx.moveTo(0, 55);
    ctx.lineTo(300, 55);
    ctx.stroke(); // 进行绘制
    // 绘制图片的时候没有问题，在导出画布的时候，就会失败，因为外部链接，会导致污染
    let img = await getImage(data.cover);
    ctx.drawImage(img, 50, 70, 200, 200);

    // 绘制图片下边的文字
    ctx.font = '12px Georgia';
    ctx.fillStyle = '#000';
    ctx.fillText(data.title, 50, 300);
    ctx.fillText(data.summary, 50, 330);

    // 绘制二维码
    const qrcode = document.getElementById('qrcode');
    const qrcodeimg = await getImage(qrcode.toDataURL());
    ctx.drawImage(qrcodeimg, 50, 350, 80, 80);

    // 绘制二维码后面的文字
    ctx.fillStyle = '#000';
    ctx.fillText(data.title, 190, 350);

    // 绘制二维码后面的图片
    ctx.fillText(data.title, 190, 390);
    // 生产url  把canvas转成图片
    const imgUrl = await canvasDom.current.toDataURL();
    // console.log(imgUrl, 'imgUrl');
    return imgUrl;
};
const gitInitCanvas = async (data, canvasDom) => {
    canvasDom.current.width = 400;
    canvasDom.current.height = 470;
    const ctx = canvasDom.current.getContext('2d'); //上下文对象
    let imgurl = await drawCanvas(ctx, data, canvasDom);
    // console.log(imgurl,"3155546464");
    // setUrl(imgurl)
    return imgurl;
};
const useCanvas = (data, canvasDom) => {
    const [url, seturl] = useState('');
    useEffect(() => {
        (async () => {
            let url = await gitInitCanvas(data, canvasDom);
            seturl(url);
        })();
    }, [data]);
    return url;
};

export default useCanvas;
