import { useState, useEffect } from "react"


const useRequest = (service, options) => {
    const [list, setList] = useState([])
    const [error, setError] = useState({})
    const getList = async () => {
        let res = await service(options?.data);
        if (res.data?.statusCode == 200) {
            setList(res.data?.data || [])
        } else {
            setError(res.data?.error)
        }
    }
    const run = () => {
        getList();
    }


    useEffect(() => {
        if (!options?.manual) {
            getList();
        }
    }, []);


    return {
        list,
        error,
        run
    }
}

export default useRequest