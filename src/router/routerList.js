import { lazy } from 'react';
// export const routerschilrens = [
//     {
//         path: '/home/homeIndex/category/:id',
//         components: lazy(() => import('../views/home/HomeIndex/category/all')),
//         title: 'all',
//     },
// ];
export const routerschil = [
    {
        path: '/home/homeIndex/category',
        components: lazy(() =>
            import('../views/home/HomeIndex/index')
        ),
        title: 'home',
        // children: routerschilrens
    },
    {
        path: '/home/list',
        components: lazy(() =>
            import('../views/home/List')
        ),
        title: 'article',
    },
    {
        path: '/home/Friends',
        components: lazy(() =>
            import('../views/home/Friends')
        ),
        title: 'file',
    },
    {
        path: '/home/Houseofhello',
        components: lazy(() =>
            import('@/views/home/Houseofhello')
        ),
        title: 'konwledge',
    },
    /* {
        path: '/home/messageboard',
        components: lazy(() =>
            import('../views/home/MessageBoard/MessageBoard')
        ),
        title: 'messageboard',
    }, */
    {
        path: '/home/kenowledge/:id',
        components: lazy(() => import('../views/home/Kenowledge/kenowledge')),
        // children: routerschil,
    },
    {
        path: '/home/Tatle',
        components: lazy(() => import('../views/home/Tatle/index')),
    }
];
let routers = [
    {
        path: "/",
        redirect: "/home/homeIndex/category"
    },
    {
        path: '/home',
        components: lazy(() => import('../views/home')),
        children: routerschil,
    },
    {
        path: '/home/Detail',
        components: lazy(() => import('../views/home/Detail/index')),
    },
    
    {
        path: '*',
        components: lazy(() => import('../views/P404')),
    },

];
export default routers;
