import React, { Suspense } from 'react';
import routers from './routerList';
import {
    Navigate,
    Route,
    Routes,
    BrowserRouter,
} from 'react-router-dom';

function RouterViews() {
    const renderDom = (routers) => {
        return routers.map((item, index) => {
            return (
                <Route
                    key={index}
                    path={item.path}
                    element={
                        item.redirect ? (
                            <Navigate to={item.redirect} />
                        ) : (
                            <item.components />
                        )
                    }
                >
                    {item.children &&
                        renderDom(item.children)}
                </Route>
            );
        });
    };

    return (
        <Suspense fallback={<div>...loading</div>}>
            <BrowserRouter>
                <Routes>{renderDom(routers)}</Routes>
            </BrowserRouter>
        </Suspense>
    );
}

export default RouterViews;
