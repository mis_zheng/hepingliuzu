const Tag = {
    getTag: {
        method: 'get',
        url: '/api/tag',
    },
    getArticle:{
        method:'get',
        url:'/api/article'
    },
    getTitleList:{
        method:'get',
        url:'/api/category'
    },
    getBrrList:{
        method:'get',
        url:'/api/Knowledge'
    },
    getArrList:{
        method:'get',
        url:'/api/article/recommend'
    }
};
export default Tag;
