const category={
    getCategory :{
        method:"get",
        url:"/api/category"
    },
    getComment :{
        method:"get",
        url:"/api/comment/host/"
    },
    getRecommendid:{
        method:"get",
        url:`/api/article/category/`
    }
}
export default category