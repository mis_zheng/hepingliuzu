import request from "../utils/request";
import httpTool from "../utils/httpTool";
import {message} from "antd";
//  路径是否需要递归 规则
const context = require.context("./modules", false, /\.js$/);
const httpAxios=httpTool({
  timeout:10,
  commonHeader:{
    token:localStorage.getItem("token")||'123'
  },
  failMessage:(msg)=>{
    message.warning(msg)
  }
})
const dataObj = context.keys().reduce((prev, cur) => {
    const def = {...prev,...context(cur).default};

    return def

}, {});

const api =Object.keys(dataObj).reduce((prev, cur) => {
    prev[cur]=(data,id)=>{
        return  httpAxios({
          ...dataObj[cur], 
          url:id?dataObj[cur].url+id:dataObj[cur].url,
        [ dataObj[cur].method==="get"?"get":"data"]:data
        })
    }
    return prev
},{});
  export default api





