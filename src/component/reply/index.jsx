import { Button, Space } from 'antd';
import React, { useState } from 'react';
import reply from "./reply.module.less"

function Reply() {

    const [loadings, setLoadings] = useState([]);

    const enterLoading = (index) => {
     setLoadings((prevLoadings) => {
          const newLoadings = [...prevLoadings];
          newLoadings[index] = true;
          return newLoadings;
        });
     
    }
    return (
        <div className={reply.comment}>
            <textarea cols="61" rows="5"></textarea>
        <Button type="primary" className={reply.antbtns} loading={loadings[0]} onClick={() => enterLoading(0)}>
         提交
        </Button>
        </div>
    );
}

export default Reply;
