import React,{useState} from 'react';
import pagination from './pagination.module.less';


function Pagination({ total, page = 1, pageSize = 5,handlePage }) {
    const count = Math.ceil(total / pageSize);
    const handpageInfo=(page)=>{
      handlePage(page)
    }



    return (
        <div className={pagination.pagination}>
            <div>
                {new Array(count).fill(count).map((item, index) => {
                    return <span key={index} className={page===index+1?pagination.active:""} 
                            onClick={()=>handpageInfo(index+1)}  >{index + 1}</span>;
                })}
            </div>
        </div>
    );
}

export default Pagination;
