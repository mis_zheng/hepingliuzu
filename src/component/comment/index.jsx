import React,{useCallback} from 'react';
import CommentMdule from './comment.module.less';
import * as dayjs from 'dayjs'
import * as relativeTime from 'dayjs/plugin/relativeTime' // 导入插件
import 'dayjs/locale/zh-cn' // 导入本地化语言
import {randomColor} from "@/utils/color"

dayjs.extend(relativeTime) // 使用插件
dayjs.locale('zh-cn') // 使用本地化语言
function Comment({ list }) {
 
    return (
        <>
            {list &&
                list.map((item) => {
                    return (
                        <div className={CommentMdule.comment} key={item.id}>
                            <div className={CommentMdule.header}>
                                <span style={{background:randomColor()}}>{item.name.slice(0,1)}</span>
                                <div>
                                    <span>{item.name}</span> 
                                    {
                                      item.replyUserName&&<div>回复 {item.replyUserName}</div>
                                    }
                                    
                                </div>
                            </div>

                            <div className={CommentMdule.content}>{item.content}</div>
                            <div className={CommentMdule.footer}>
                                <span>{item.userAgent}</span>
                                <span>{dayjs(new Date(item.updateAt)).fromNow() }</span>
                                <span>回复</span>
                               
                            </div>
                            {
                              item.children&&<Comment list={item.children} />
                            }
                        </div>
                    );
                })}
        </>
    );
}

export default Comment;
