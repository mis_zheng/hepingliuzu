import React, { useState ,useRef} from 'react';
import { Button, Form, Input, message } from 'antd';
function Login({handleOk}) {
    const [form] = Form.useForm();
    const [count,setcount]=useState(1)
    const disab=useRef(null)
    const onFinish = (values) => {
        if (values.username === 'admin' && values.password === '123') {
            window.localStorage.setItem('token', JSON.stringify(values));
            handleOk();
            setcount(1)
        }else{
            form.setFieldsValue({
                username:"",
                password:""
              })
              message.error('尊敬的用户，输入错误，请重新尝试');
              setcount(count+1)
              if(count>=2){
                disab.current.disabled="disabled"
                message.warning (`尊敬的用户，输入错误，请${count}秒后重新尝试`);
              }
              setTimeout(()=>{
                disab.current.disabled=""
              },count*1000)
        }
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div>
           <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                    form={form}
                >
                    <Form.Item
                        label="用户名"
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="密码"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit" ref={disab}>
                            登录
                        </Button>
                        <Button type="primary" onClick={handleOk}>
                            取消
                        </Button>
                    </Form.Item>
                </Form>
        </div>
    );
}

export default Login;
