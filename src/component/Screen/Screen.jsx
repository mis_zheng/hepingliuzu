
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import api from '@/api';
import Items from '../Items/Items';
import {useNavigate} from "react-router-dom";
function Screen({setflag}) {
    const [lists, setlists] = useState([]);
    const [active, setactive] = useState(0);
    const [value, setvalue] = useState([]);
    const dispatch = useDispatch();
    const navigate=useNavigate()
    const getarticle = async () => {
        let res = await api.getCategory();
        // 创建全部的分支
        let obj = {
            articleCount: 1,
            createAt: '2022-08-09T11:06:59.302Z',
            id: '',
            label: 'all',
            updateAt: '2022-09-13T08:17:07.604Z',
            value: 'all',
        };
        res.data.data.unshift(obj);
        setlists(res.data.data);
    };
    // 全部数据
    const getRecommends=async()=>{
        let res = await api.getRecommend();
        setvalue(res.data.data)
    }
    // 筛选数据
    const getRecommendids=async(value)=>{
        let res = await api.getRecommendid({},value);
        setvalue(res.data.data[0])
      
    }
    const routertab = (index, value,label) => {
        setactive(index);
        if(value=="all"){
            getRecommends()
            setflag(true)
            navigate('/home/homeIndex/category')
        }else{
            // getRecommendids(value)
            setflag(false)
           let arr= lists.filter(item=>item.label==label)
            dispatch({
                type: 'getcategory',
                payload:arr,
            });
            navigate(`/home/kenowledge/` +value,{state:{arr:arr,value:value}})
        }
    };
    useEffect(() => {
        getarticle();
        getRecommends()
    }, []);
    return (
        <div className="screen_both">
            <div className="nav_both">
                <div>
                    {lists &&
                        lists.map((item, index) => {
                            return (
                                <li
                                    key={index}
                                    
                                    className={
                                        index === active ? 'on ss' : 'ss'
                                    }
                                    onClick={() => routertab(index, item.value,item.label)}
                                >
                                    {item.label}
                                </li>
                            );
                        })}
                </div>
                {value && value.length > 0 ? (
                    value.map((item, index) => {
                        return <Items key={index} item={item}></Items>;
                    })
                ) : (
                    <div className="screen_empty">
                        <span>empty</span>
                    </div>
                )}
            </div>
        </div>
    );
}

export default Screen;

