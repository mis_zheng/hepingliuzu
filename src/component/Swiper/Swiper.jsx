import React,{useEffect} from "react";
import { Carousel } from "antd";
function Swiper({ list }) {
    list.forEach(((item,index)=>{
        if(!item.cover){
            list.splice(index,1)
        }
      }))
    return (
        <div className="slick-one">
            <div className="slick-two">
                <Carousel autoplay>
                    {list &&
                        list.map((item, index) => {
                            return (
                                <div
                                    key={index}
                                    className="contentStyle"
                                >
                                    <img
                                        src={item.cover}
                                        alt=""
                                        className="swiper"
                                    />
                                    <div className="slick-three">
                                        {item.title}
                                    </div>
                                </div>
                            );
                        })}
                </Carousel>
            </div>
        </div>
    );
}

export default Swiper;
