import React from 'react'
import Timess from '../Timess/Timess';
import Labels from '../Labels/Labels';
import Packaging from "@/component/Packaging/packging"
import {useNavigate} from "react-router-dom"
function Items({ item }) {
  const usenav=useNavigate()
  const itembtn=()=>{
    usenav("/home/Detail",{state:item})
  }
  return (
    <div className="screen-box">
          <div className="screen_disflex" onClick={itembtn}>
            <h2>{item.title}</h2>
            <span>|</span>
            <Timess item={item} className="screen_times"></Timess>
            <span>|</span>
            <Labels item={item} className="screen_labels"></Labels>
          </div>

          <dl className="item-flex">
            <dt>
             
                 {item.cover ? (
                <img src={item.cover} alt="" className="img" />
              ) : null}

             
            </dt>
            <dd className='Adjust'>
              <p>{item.summary}</p>
              <Packaging list={item} />
            </dd>
          </dl>
      
    </div>
  );
}

export default Items;
