
import React,{useState,useEffect,useMemo}from 'react';
import { Spin } from 'antd';
import { useSelector } from 'react-redux';
import {useNavigate} from "react-router-dom";
import { useTranslation } from 'react-i18next'
import i18n from 'i18next';
import style from "./right.module.less"
import Timess from '../Timess/Timess';
function Right(props) {
    console.log(style);
    let [flag,setflag]=useState(true);
    const { t } = useTranslation()
    const usenav=useNavigate();
  useMemo(()=>{
    if(props.rlist?.length>0){
        flag=!flag
        setflag(flag)
    }
  },[props.rlist])
    
    const val=useSelector((state)=>{
        return state.val
    })
    useEffect(()=>{
        i18n.changeLanguage(val); 
    },[val])
  
    return (
        <div className="rights">
                <h4>{t('header.recommendToReading')}</h4>
                <Spin spinning={flag}>
                    <ul className={style.cartoon}>
                        {
                            props.rlist&&props.rlist?props.rlist.map((item,index)=>{
                                if(index<5){
                                    return <li key={index} onClick={()=>{
                                    usenav("/home/Detail",{state:item})
                                }}  
                                style={{animation:`${style.movens} ${ (index + 1) * 0.6}s ease-out ${(index + 2) * 0.1}s backwards`}}
                                >{item.title}
                                <span>大约<Timess item={item}/></span>
                                </li>
                                }
                                
                            }):"暂无数据"
                        }
                    </ul>
                </Spin>
                
        </div>
    );
}

export default Right;
