import React from 'react';
import { useSelector } from 'react-redux';
import ItemDetails from './style/ItemDetail.module.less';
function ItemDetail() {
    const { category } = useSelector((state) => {
        return state;
    });
    return (
        <div className={ItemDetails.ItemDetail}>
            {category &&
                category.map((item, index) => {
                    return (
                        <div key={index}>
                            <div>
                                <b className={ItemDetails.ss1}>
                                    {category[0].label}
                                </b>
                                <span>categoryArticle</span>
                            </div>
                            <div>
                                <span>totalSearch</span>
                                <span className={ItemDetails.ss}>
                                    {category[0].articleCount}
                                </span>
                                <span>piece</span>
                            </div>
                        </div>
                    );
                })}
        </div>
    );
}

export default ItemDetail;
