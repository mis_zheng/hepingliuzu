import {useSelector} from "react-redux";
import React,{useEffect}from 'react';
import { useTranslation } from 'react-i18next'
import i18n from 'i18next';
import {useNavigate} from "react-router-dom"
function Rightbum({list}){
    const { t } = useTranslation()
    const val=useSelector((state)=>{
        return state.val
    })

    useEffect(()=>{
        i18n.changeLanguage(val); 
    },[val])
    const usenav=useNavigate()
   
    return(
            <div className="rightbum">
                <h4>{t('header.tagTitle')}</h4>
                <ul>
                    {
                        list&&list.map((item,index)=>{
                            return <li key={index} onClick={()=>{
                                usenav("/home/Tatle",{state:item})
                            }}>{item.label}</li>
                        })
                    }
                </ul>
            </div>
    )
}


export default Rightbum