
import useRequest from "@/hook/useRequest";
import React, { useState, useEffect } from 'react';
import {useDispatch } from 'react-redux';
import { NavLink,useNavigate} from 'react-router-dom';
import api from '@/api';
// import Items from '../Items/Items';
import '@/style/home.less';
function RightBottom() {
    const lists = (useRequest(api.getcategory)).list;
    const [active, setactive] = useState(0);
    const [value, setvalue] = useState([]);
    const usenavigate=useNavigate()
    const dispatch = useDispatch();
    
    //全部数据
    // const getRecommends=async()=>{
    //     let res = await api.getRecommend();
    //     setvalue(res.data.data)
    // }
    ///筛选数据
    // const getRecommendids=async(value)=>{
    //     let res = await api.getRecommendid({},value);
    //     setvalue(res.data.data[0])
      
    // }
    const routertab = (index, value,label) => {
        // setactive(index);
        //     getRecommendids(value)
           let arr= lists.filter(item=>item.label==label)
            dispatch({
                type: 'getcategory',
                payload:arr,
            });
            // console.log(arr);
        usenavigate(`/home/kenowledge/` +value,{state:{arr:arr,value:value}})
    };
   
    // console.log(value);
    return (
        <div className="screen_both">
            
            <div className="nav_both">
                 <h4>Category</h4>
                <ul className="lis">
               
                        {lists &&
                            lists.map((item, index) => {
                                return (
                                    
                                    <li
                                        key={index}
                                        onClick={() => routertab(index, item.value,item.label)}
                                    >
                                        {item.label}
                                    </li>
                                );
                            })}
                </ul>
            </div>
        </div>
    );
}

export default RightBottom;

