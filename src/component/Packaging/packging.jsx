import React,{ useState } from 'react'
import { Modal,Button,message } from 'antd';
import { PartitionOutlined } from '@ant-design/icons';
import QRCode from 'qrcode.react';
import "@/style/encapsulation.less"
function Packging({list}) {
    const [isModalOpen, setIsModalOpen] = useState(false);
    // list.forEach(((item,index)=>{
    //     console.log(item,"2222");
    //     })
    // );
  const showModal = () => {
    setIsModalOpen(true);
  };
  const [loadings, setLoadings] = useState([]);
  const handleOk = () => {
    
    const hide = message.loading('shareNamespace.createingPoster', 0); // Dismiss manually and asynchronously
    setTimeout(() => {
      message.error('createdPosterError');
      message.error('shareNamespace.createdPosterError');
      setIsModalOpen(false);
    },4000);
  setTimeout(hide, 3000);
  };

    const handleCancel = () => {
        setIsModalOpen(false);
    };
    return (
 
            <>
                <div className="share">
                    <svg
                        viewBox="64 64 896 896"
                        focusable="false"
                        data-icon="heart"
                        width="1em"
                        height="1em"
                        fill="currentColor"
                        aria-hidden="true"
                    >
                        <path d="M923 283.6a260.04 260.04 0 00-56.9-82.8 264.4 264.4 0 00-84-55.5A265.34 265.34 0 00679.7 125c-49.3 0-97.4 13.5-139.2 39-10 6.1-19.5 12.8-28.5 20.1-9-7.3-18.5-14-28.5-20.1-41.8-25.5-89.9-39-139.2-39-35.5 0-69.9 6.8-102.4 20.3-31.4 13-59.7 31.7-84 55.5a258.44 258.44 0 00-56.9 82.8c-13.9 32.3-21 66.6-21 101.9 0 33.3 6.8 68 20.3 103.3 11.3 29.5 27.5 60.1 48.2 91 32.8 48.9 77.9 99.9 133.9 151.6 92.8 85.7 184.7 144.9 188.6 147.3l23.7 15.2c10.5 6.7 24 6.7 34.5 0l23.7-15.2c3.9-2.5 95.7-61.6 188.6-147.3 56-51.7 101.1-102.7 133.9-151.6 20.7-30.9 37-61.5 48.2-91 13.5-35.3 20.3-70 20.3-103.3.1-35.3-7-69.6-20.9-101.9zM512 814.8S156 586.7 156 385.5C156 283.6 240.3 201 344.3 201c73.1 0 136.5 40.8 167.7 100.4C543.2 241.8 606.6 201 679.7 201c104 0 188.3 82.6 188.3 184.5 0 201.2-356 429.3-356 429.3z"></path>
                    </svg>
                    <span>·</span>
                    <span>{list.likes}</span>
                    <span>·</span>
                    <svg
                        viewBox="64 64 896 896"
                        focusable="false"
                        data-icon="eye"
                        width="1em"
                        height="1em"
                        fill="currentColor"
                        aria-hidden="true"
                    >
                        <path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path>
                    </svg>
                    <span>·</span>
                    <span>{list.views}</span>
                    <span>·</span>
                    <PartitionOutlined onClick={showModal} />
                    share
                    <Modal
                        title="shareNamespace.title"
                        width={400}
                        open={isModalOpen}
                        footer={null}
                        onCancel={handleCancel}
                    >
                        <div>
                            <img
                                src={list.cover}
                                height={218.25}
                                alt=""
                                width={'100%'}
                            />
                            <h3>{list.title}</h3>
                            <div className="center">
                                <QRCode
                                    value="https://www.baidu.com/" // 生成二维码的内容
                                    size={80} // 二维码的大小
                                    fgColor="#000000" // 二维码的颜色
                                    imageSettings={{
                                        // 中间有图片logo
                                        height: 60,
                                        width: 60,
                                        excavate: true,
                                    }}
                                />
                                <div>
                                    <p className="sixteen">
                                        shareNamespace.qrcode
                                    </p>
                                    <p className="em">
                                        shareNamespace.shareFrom
                                        <span className="red">sunscine</span>
                                    </p>
                                </div>
                            </div>
                            <div className="RightPlace">
                                <Button onClick={() => handleCancel()}>
                                    关闭
                                </Button>
                                <Button
                                    type="primary"
                                    danger
                                    onClick={() => handleOk()}
                                />
                                <Button
                                    type="primary"
                                    loading={loadings[0]}
                                    onClick={() => handleOk()}
                                >
                                    下载
                                </Button>
                            </div>
                        </div>
                    </Modal>
                </div>
           
        </>
    
  )
}

{/* <svg className='dh' viewBox="0 0 1024 1024" focusable="false" data-icon="loading" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M988 548c-19.9 0-36-16.1-36-36 0-59.4-11.6-117-34.6-171.3a440.45 440.45 0 00-94.3-139.9 437.71 437.71 0 00-139.9-94.3C629 83.6 571.4 72 512 72c-19.9 0-36-16.1-36-36s16.1-36 36-36c69.1 0 136.2 13.5 199.3 40.3C772.3 66 827 103 874 150c47 47 83.9 101.8 109.7 162.7 26.7 63.1 40.2 130.2 40.2 199.3.1 19.9-16 36-35.9 36z"></path></svg> */}

export default Packging