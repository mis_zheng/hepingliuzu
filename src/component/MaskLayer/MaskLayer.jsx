import React from 'react';
import style from './MaskLayer.module.less';
import {useNavigate} from "react-router-dom"
import axios from "axios"
function MaskLayer() {
    const navigate=useNavigate()
    const MaskLayerBack=()=>{
        navigate('/')
    }
    const handlepay=async ()=>{
        let res=await axios.get('http://localhost:7000/pay')
        console.log(res);
        window.location.href=res.data.result
    }
    return (
        <div className={style.MaskLayerBox}>
            <div className={style.MaskLayerTan}>
                <p className={style.MaskLayerH}>
                <span className={style.MaskLayerSpan1}>文章受保护，请输入访问密码</span><span className={style.MaskLayerSpan2} onClick={MaskLayerBack}>x</span>
                </p>
                <p className={style.MaskLayerH1}>密码：<input type="text" className={style.MaskLayerIpu}/></p>
                <p className={style.MaskLayerH2}>
                    <button className={style.MaskLayerBtn1} onClick={MaskLayerBack}>返回首页</button>
                    <button className={style.MaskLayerBtn2} onClick={handlepay}>确定</button>
                </p>
            </div>
        </div>
    );
}
export default MaskLayer;
