import React from 'react';
import "../../style/fand/nofand.less";
import { Button } from 'antd';
import {useNavigate} from "react-router-dom";
function P404() {
    const usenav=useNavigate()
    return <div className='nofand'>
        <div className='fand'>
            <img src="https://i.pinimg.com/736x/21/00/a7/2100a781f929678be9e38d47b1ab0e11--honey-posts.jpg" alt="" />
            <Button type="primary" danger ghost onClick={()=>{
                usenav("/home/homeIndex/category")
            }}>
                    点击返回
            </Button>
        </div>
        
    </div>;
}

export default P404;
