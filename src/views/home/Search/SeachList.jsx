import React from 'react'
import { Input, Space } from 'antd';
import Items from '@/component/Items/Items';
import {useSelector,useDispatch} from "react-redux"
import style from "./seachList.module.less"
function SeachList({data}) {
  const dispatch=useDispatch()
  const { Search } = Input;
  const back=()=>{
    data(false)
  }
  const onSearch = (value) =>{
    dispatch({
      type:"searchlist",
      payload:value
    })
  };
  const list=useSelector(state=>{
    return state.searchlist
  })
  return (
    <div className='searchlist'>
      <div className='container'>
        <header>
          <span className='searchtitle'>
            searchArticle
          </span>
          <span onClick={()=>back()} className="" >
              <svg className='svgimg' viewBox="64 64 896 896" focusable="false" data-icon="close" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 00203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z"></path></svg>
              <span className='svgimg'>esc</span>
          </span>
        </header>
        <main>
        <Space direction="vertical">
          <Search  placeholder="searchArticlePlaceholder" allowClear onSearch={onSearch}  />
        </Space>
        </main>
        <div className={style.main_box}>
          {
              list&&list.length>0?list.map((item,index)=>{
                    return <Items key={index} item={item}></Items>
                  }):<div className='screen_empty'>
                  <span>
                    empty
                  </span>
              </div>
          }
        </div>
      </div>
    </div>
  )
}

export default SeachList
