import React, { useState, useEffect, useRef, useCallback } from 'react';
import './markdown/markdown.less';
import { useLocation, useNavigate } from 'react-router-dom';
import './markdown/detail.less';
import useRequest from '@/hook/useRequest.js';
import api from '@/api/index';
import Up from '@/component/Up';
import QRCode from 'qrcode.react';
import useCanvas from '@/hook/useCanvas';
import Reply from '@/component/reply';
import Comments from '@/component/comment';
import Pagination from '@/component/pagination';
import { Modal, Button } from 'antd';
import konwledge from './konwledge.json';
import Taoxin from '@/icon/Taoxin';
import Pinglun from '@/icon/Pinglun';
import Fengxing from '@/icon/Fengxing';
import Right from "@/component/Right/Right"
function Index() {
    const location = useLocation();
    const { list } = useRequest(api.getRecommend);
    const canvasDom = useRef(null);
    const [data, setData] = useState({});
    const [commentList, setCommentList] = useState([]);
    const [flag, setflag] = useState(false); //画布按钮的显示隐藏
    const url = useCanvas(data, canvasDom);
    const konwledge = useRef(null);
    const [active, setactive] = useState(0);
    const scrollH = useRef(null); // 每一个标题距离页面顶部的距离
    const hs = ['h1', 'h2', 'h3'];
    const [oneflag, setoneflag] = useState(false);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [pageInfo, setpageInfo] = useState({
        page: 1,
        pageSize: 5,
    });
    const navigate = useNavigate();
    const changeactive = (index) => {
        setactive(index);
        document.documentElement.scrollTop = scrollH.current[index];
    };
    const scollHandler = () => {
        const scrolY = Math.ceil(document.documentElement.scrollTop); //滚动距离
        scrollH.current.map((item, index) => {
            if (scrolY >= item && scrolY < scrollH.current[index + 1]) {
                setactive(index);
            }
        });
    };
    useEffect(() => {
        //获取所有h1  h2 h3标签
        const child = [...konwledge.current.children].filter((item) =>
            hs.includes(item.nodeName.toLowerCase())
        );
        // 获取h标签距离顶部的距离
        scrollH.current = child.map((item) => item.offsetTop);
        // 获取最后一个h标签
        const lastChild = child[child.length - 1];
        lastChild &&
            scrollH.current.push(lastChild.offsetTop + lastChild.offsetHeight);
        window.addEventListener('scroll', scollHandler, false);
        // 清除滚动事件
        return () => {
            window.removeEventListener('scroll', scollHandler, false);
        };
    }, [data]);
    const handleShare = () => {
        setflag(!flag);
        setIsModalOpen(true);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };
    const getCommentList = async () => {
        const res = await api.getComment(pageInfo, location.state.id);
        if (res.data.statusCode === 200) {
            setCommentList(res.data.data);
        }
    };
    const [count, setCount] = useState(0);
    useEffect(() => {
        setCount(location.state.views);
    }, []);
    const handleflag = () => {
        if (oneflag) {
            setCount(count - 1);
            setoneflag(false);
        } else {
            setCount(count + 1);
            setoneflag(true);
        }
    };
    useEffect(() => {
        setData(location.state);
        getCommentList();
    }, []);

    const handlePage = useCallback(
        (page) => {
            setpageInfo({
                ...pageInfo,
                page,
            });
        },
        [setpageInfo]
    );
    const gotochat = () => {
        window.scrollBy(0, 1600);
    };
    return (
        <div className="markdown detail_main">
            <div className="detail_left">
                <img src={location.state.cover} alt="" className="imgs" />
                <div
                    ref={konwledge}
                    className="left_detail"
                    dangerouslySetInnerHTML={{ __html: location.state.html }}
                ></div>
                {/* 海报 */}
                <canvas
                    id="canvas"
                    ref={canvasDom}
                    className="KnowModule"
                ></canvas>
                <QRCode
                    id="qrcode"
                    value={window.location.href} //value参数为生成二维码的链接 我这里是由后端返回
                    className="KnowModule"
                    size={200} //二维码的宽高尺寸
                />
                <Modal
                    width={400}
                    open={isModalOpen}
                    onCancel={handleCancel}
                    footer={null}
                >
                    <div>
                        {flag && (
                            <div className="dialog">
                                <img src={url} />
                                {/* <button onClick={()=>handleDownloader()}>下载</button> */}
                            </div>
                        )}
                        <div className="RightPlace">
                            <Button>
                                <a href={url} download="canvas.png">
                                    下载
                                </a>
                            </Button>
                            <Button onClick={() => handleCancel()}>关闭</Button>
                        </div>
                    </div>
                </Modal>

                {/* 评论 */}
                <Reply></Reply>
                {/* 评论列表 */}
                {commentList[1] > 0 ? (
                    <Comments
                        list={
                            commentList &&
                            commentList[0].slice(
                                (pageInfo.page - 1) * pageInfo.pageSize,
                                pageInfo.page * pageInfo.pageSize
                            )
                        }
                    />
                ) : (
                    ''
                )}
                {/* 分页 */}
                {commentList[1] > 0 ? (
                    <Pagination
                        total={commentList && commentList[1]}
                        handlePage={handlePage}
                        page={pageInfo.page}
                        pagesize={pageInfo.pagesize}
                    />
                ) : (
                    ''
                )}

                <Up></Up>

                <div className="leftfixed">
                    <div>
                        <span>
                            <Taoxin />
                        </span>
                        <p className="Smallround">{count}</p>
                    </div>
                    <div>
                        <span>
                            <Pinglun onClick={() => gotochat()} />
                        </span>
                    </div>
                    <div>
                        <span onClick={() => handleShare()}>
                            <Fengxing onClick={() => handleflag()} />
                        </span>
                    </div>
                </div>
            </div>
            <div className="detail_right">

               <Right rlist={list} />

                <div className="right_detail">
                    {data.toc &&
                        JSON.parse(data.toc).map((item, index) => (
                            <p
                                className={active == index ? 'active' : ''}
                                key={item.id}
                                onClick={() => changeactive(index)}
                            >
                                {item.text}
                            </p>
                        ))}
                </div>
            </div>
        </div>
    );
}

export default Index;
