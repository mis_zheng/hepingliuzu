import { Button, Layout, Modal } from 'antd';
import { SearchOutlined, UnorderedListOutlined } from '@ant-design/icons';
import React, { useState, useEffect, useMemo } from 'react';
import { routerschil } from '@/router/routerList';
import { NavLink, Outlet, useNavigate } from 'react-router-dom';
import '@/style/variable.less';
import '@/style/home.less';
import Taiyang from '@/icon/Taiyang';
import Yueliang from '@/icon/Yueliang';
import Zhongwen from '@/icon/Zhongwen';
import Yingwen from '@/icon/Yingwen';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import i18n from 'i18next';
import Login from '@/component/Login/login.jsx';
import SearchList from './Search/SeachList'
const { Header, Content, Footer } = Layout;
function App() {
    const token = window.localStorage.getItem('token');
    const Navigate = useNavigate();
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const [china, setChina] = useState(true);
    const [daytime, setDaytime] = useState(true);
    const [screen, setscreen] = useState(false);
    const [pos, setpos] = useState(false);
    const [screens, setscreens] = useState(999);
    const [searchflag, setsearchflag] = useState(false);
    const [val, setval] = useState('en');
    const [isModalOpen, setIsModalOpen] = useState(false);
    // 点击头像跳转到首页
    const clickHome = () => {
        Navigate('/home/homeIndex/category');
    };
    // 中英文
    const clickC = () => {
        if (china) {
            setChina(false);
            setval('zh');
        } else {
            setChina(true);
            setval('en');
        }
        setpos(false);
        dispatch({
            type: 'setval',
            payload: val,
        });
    };

    // 白天夜间模式
    const clickT = () => {
        setDaytime(!daytime);
        setpos(false);
        dispatch({
            type: 'setDaytime',
            payload: daytime,
        });
    };
    useEffect(() => {
        i18n.changeLanguage(val);
    }, [val]);
    useEffect(() => {
        const root = document.documentElement;
        root.className = daytime ? 'light' : 'dark';
    }, [daytime]);

    window.onresize = function () {
        setscreens(window.innerWidth);
    };
    useMemo(() => {
        if (screens < 900) {
            setscreen(true);
        } else if (screens > 900) {
            setscreen(false);
        }
    }, [screens]);
    const searchArticle = () => {
        setsearchflag(!searchflag);
    };
    const clickPos = (event) => {
        setpos(!pos);
    };

    const showModal = () => {
        setIsModalOpen(true);
    };

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    return (
        <>
        <Layout>
            <Modal
                title="Basic Modal"
                open={isModalOpen}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={null}
            >
                <Login handleOk={handleOk} />
            </Modal>
            {
                searchflag?<SearchList data={setsearchflag}/>:''
            }
            <Header
                style={{
                    position: 'fixed',
                    zIndex: 1,
                    width: '100%',
                }}
            >
                <div className="header">
                    <div className="left">
                        {token ? (
                            <img
                                src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
                                alt=""
                                width={50}
                                height={40}
                                onClick={() => clickHome()}
                            />
                        ) : (
                            <div className="lefts" onClick={showModal}>
                                未登录
                            </div>
                        )}
                    </div>
                    {screen ? (
                        ''
                    ) : (
                        <ul className="menu">
                            {routerschil.map((item, index) => {
                                if(item.title){
                                 return (
                                    <li key={index}>
                                        <NavLink to={item.path} key={index}>
                                            {item.title
                                                ? t('header.' + item.title)
                                                : null}
                                        </NavLink>
                                    </li>
                                )
                                }
                                
                            })}
                        </ul>
                    )}

                    {screen ? (
                        <div className="rights">
                            <div onClick={(e) => clickPos(e)}>
                                <UnorderedListOutlined />
                            </div>
                            {pos ? (
                                <div className="pos">
                                    <ul>
                                        {screen
                                            ? routerschil.map((item, index) => {
                                                if(item.title){
                                                    return (
                                                        <li
                                                         key={index}
                                                         className="poss"
                                                     >
                                                         <NavLink
                                                             to={item.path}
                                                             key={index}
                                                         >
                                                             {item.title}
                                                         </NavLink>
                                                     </li>
                                                 );
                                                }
                                                  
                                              })
                                            : ''}
                                        <li
                                            onClick={() => clickC()}
                                            className="poss"
                                        >
                                            {china ? <Zhongwen /> : <Yingwen />}
                                        </li>
                                        <li
                                            onClick={() => clickT()}
                                            className="poss"
                                        >
                                            {daytime ? (
                                                <Taiyang />
                                            ) : (
                                                <Yueliang />
                                            )}
                                        </li>
                                        <li className="poss">
                                            <SearchOutlined onClick={()=>searchArticle()} />
                                        </li>
                                    </ul>
                                </div>
                            ) : (
                                ''
                            )}
                        </div>
                    ) : (
                        <div className="right">
                            <div onClick={() => clickC()}>
                                {china ? <Zhongwen /> : <Yingwen />}
                            </div>
                            <div onClick={() => clickT()}>
                                {daytime ? <Taiyang /> : <Yueliang />}
                            </div>
                            <div>
                                <SearchOutlined onClick={()=>searchArticle()} />
                            </div>
                        </div>
                    )}
                </div>
            </Header>

            <Content>
                <Outlet />
            </Content>

             <Footer
                style={{
                    textAlign: 'center',
                    backgroundColor:"#292525",
                    color:"#fff"
                }}
            >
                <a href="https://creation.shbwyz.com/rss">
                <svg t="1664107835576"  viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3780" width="25" height="25"><path d="M505.6 337.066667c-115.2 0-221.866667 46.933333-298.666667 121.6l-59.733333-59.733334c78.933333-76.8 183.466667-130.133333 298.666667-142.933333 19.2-2.133333 38.4-4.266667 59.733333-4.266667s40.533333 2.133333 59.733333 4.266667c121.6 14.933333 230.4 70.4 311.466667 155.733333l-59.733333 61.866667c-78.933333-85.333333-187.733333-136.533333-311.466667-136.533333z" p-id="3781" fill="#8a8a8a"></path><path d="M505.6 529.066667c-61.866667 0-119.466667 25.6-162.133333 66.133333L283.733333 533.333333c57.6-55.466667 136.533333-89.6 221.866667-89.6 93.866667 0 179.2 40.533333 236.8 104.533334L682.666667 610.133333c-42.666667-51.2-106.666667-81.066667-177.066667-81.066666zM418.133333 667.733333c23.466667-21.333333 53.333333-34.133333 87.466667-34.133333 42.666667 0 78.933333 21.333333 102.4 51.2l-85.333333 87.466667-104.533334-104.533334z" p-id="3782" fill="#8a8a8a"></path></svg>
                </a>
                <a href="https://gitlab.com/mis_zheng/hepingliuzu">
                <svg t="1664107720874"  viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2553" width="25" height="25"><path d="M511.6 76.3C264.3 76.2 64 276.4 64 523.5 64 718.9 189.3 885 363.8 946c23.5 5.9 19.9-10.8 19.9-22.2v-77.5c-135.7 15.9-141.2-73.9-150.3-88.9C215 726 171.5 718 184.5 703c30.9-15.9 62.4 4 98.9 57.9 26.4 39.1 77.9 32.5 104 26 5.7-23.5 17.9-44.5 34.7-60.8-140.6-25.2-199.2-111-199.2-213 0-49.5 16.3-95 48.3-131.7-20.4-60.5 1.9-112.3 4.9-120 58.1-5.2 118.5 41.6 123.2 45.3 33-8.9 70.7-13.6 112.9-13.6 42.4 0 80.2 4.9 113.5 13.9 11.3-8.6 67.3-48.8 121.3-43.9 2.9 7.7 24.7 58.3 5.5 118 32.4 36.8 48.9 82.7 48.9 132.3 0 102.2-59 188.1-200 212.9 23.5 23.2 38.1 55.4 38.1 91v112.5c0.8 9 0 17.9 15 17.9 177.1-59.7 304.6-227 304.6-424.1 0-247.2-200.4-447.3-447.5-447.3z" p-id="2554" fill="#8a8a8a"></path></svg>
                </a>
                <div>index</div>
            </Footer> 
        </Layout>
        
        </>
    );
}
export default App;
