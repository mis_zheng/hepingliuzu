import React from 'react';
import api from '@/api';
import useRequest from '@/hook/useRequest';
import Right from '@/component/Right/Right';
import RightBottom from '@/component/RightBottom/RightBottom';

function List() {
     const list = (useRequest(api.getarticle)).list[0]
     const titlelist=(useRequest(api.getcategory)).list


    return (
        <div className="homeindex">
            <div className="homeindexs">
                <div className="homeindex-left">
                    <div>
                        <div className="archivesys">
                            <p className="archivesss">archives</p>
                            <p className="archivesflex">
                                <span>archives</span>
                                <span className="archives">
                                     {list?.length} 
                                </span>
                                <span>piece</span>
                            </p>
                        </div>
                        <div className="homeindexzj">
                            <h4>2022</h4>
                            <div className="homeindexzczj">
                                <h5>September</h5>
                                <ul>
                                    {list && list.length > 0 ? (
                                        list.map((item, index) => {
                                            return (
                                                <li key={index}>
                                                    <span>
                                                        {item.publishAt.slice(
                                                            5,
                                                            10
                                                        )}
                                                    </span>
                                                    <span>{item.title}</span>
                                                </li>
                                            );
                                        })
                                    ) : (
                                        <li>暂无数据</li>
                                    )}
                                </ul>
                            </div>
                        </div>
                </div>
                
            </div>
            <div className="homeindex_right">
                    <Right rlist={list} />
                    <RightBottom list={titlelist} />
                </div>
        </div>
     
     </div>
  )
}

export default List;
