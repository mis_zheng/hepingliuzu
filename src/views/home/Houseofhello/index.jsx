import React,{useState,useEffect} from 'react'
import Items from '@/component/Items/Items';
import Right from '@/component/Right/Right';
import RightBottom from '@/component/RightBottom/RightBottom';
import useRequest from '@/hook/useRequest';
import '@/style/home.less';
import api from "@/api"
function Houseofhello() {
  const [brrlist,setbrrlist]=useState([])
  const list = (useRequest(api.getArticle)).list[0];
  const brrlists=(useRequest(api.getBrrList)).list[0]
  const titlelists = (useRequest(api.getTitleList)).list
  useEffect(()=>{
    setbrrlist(brrlists)
  },[brrlists])
  return (
    <div className='homeindex'>
     <div className='homeindexs'>
        <div className='homeindex-left'>
        <div className="screen_both">
            {
              brrlist&&brrlist.length>0?brrlist.map((item,index)=>{
                return <Items key={index} item={item}></Items>
              }):<div className='screen_empty'>
                <span>
                  empty
                </span>
              </div>
            }
        </div>
       </div>
        <div className='homeindex_right'>
            <Right rlist={list}/>
            <RightBottom list={titlelists}/>
        </div>
        
     </div>
    </div>
  )
}

export default Houseofhello
