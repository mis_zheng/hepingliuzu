import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { NavLink,useLocation} from 'react-router-dom';
import api from '@/api';
import Right from '@/component/Right/Right';
import * as dayjs from 'dayjs'
import useRequest from '@/hook/useRequest';
import {useNavigate} from "react-router-dom";
import "@/style/home.less"
function Kenowledge() {
    const location= useLocation();
    const list=location.state.arr;
    const taglist=useRequest(api.getTag).list;
    const listi = (useRequest(api.getArticle)).list[0];
    const values=location.state.value;
    const [lists, setlists] = useState([]);
    const [active, setactive] = useState(0);
    const [value, setvalue] = useState([]);
    const dispatch = useDispatch();
    const navigate=useNavigate()
    const getarticle = async () => {
        let res = await api.getCategory();
        // 创建全部的分支
        let obj = {
            articleCount: 1,
            createAt: '2022-08-09T11:06:59.302Z',
            id: '',
            label: 'all',
            updateAt: '2022-09-13T08:17:07.604Z',
            value: 'all',
        };
        res.data.data.unshift(obj);
        setlists(res.data.data);
    };
   
    // 筛选数据
    const getRecommendids=async(value)=>{
        let res = await api.getRecommendid({},value);
        setvalue(res.data.data[0])
    }
    console.log(values);
    const routertab = (index, value,label) => {
        setactive(index);
        if(value=="all"){
            // getRecommends()
            navigate('/home/homeIndex/category')
        }else{
            getRecommendids(value)
           let arr= lists.filter(item=>item.label==label)
            dispatch({
                type: 'getcategory',
                payload:arr,
            });
            navigate(`/home/kenowledge/` +value,{state:{arr:arr,value:value}})
        }
    };
    useEffect(() => {
        getarticle();
        getRecommendids(values)
    }, []);
    return (
        <div className='kenow'>
            <div className='kenowTop'>
                {
                    list?list.map((item,index)=>{
                        return <div key={index} className="kenowT">
                                <h3><span>{item.label}</span>分类文章</h3>
                                <h1>共搜索到<span>{item.articleCount}</span>篇</h1>
                            </div>
                    }):"暂无数据"
                }
            </div>
            <div className='kenowR'>
                <div>
                    <Right  rlist={listi}></Right>
                </div>
                
                <div className='kenowTag'>
                <h4>tagTitle</h4>
                <ul className='kenowUl'>
                    {
                        taglist&&taglist.map((item,index)=>{
                            return <li key={index} onClick={()=>{
                                // usenav("/home/Tatle",{state:item})
                            }}>{item.label}</li>
                        })
                    }
                </ul>
                </div>
            </div>
            <div className='kenowBum'>
                <ul>
                    {lists &&
                                lists.map((item, index) => {
                                    return (
                                        <li
                                            key={index}
                                            className={
                                                index === active ? 'on ss' : 'ss'
                                            }
                                            onClick={() => routertab(index, item.value,item.label)}
                                        >
                                            {item.label}
                                        </li>
                                    );
                        })}
                </ul>
                {
                    value?value.map((item,index)=>{
                        return <div key={index} className="kenowDl">
                                <h1>{item.title}<span>大约{dayjs(new Date(item.createAt)).fromNow()}</span><span>{item.category.label}</span></h1>
                                <h2><img src={item.cover} alt="" className='img'/> <span>{item.summary}</span></h2>
                            </div>
                    }):<div className="kenowW">44545</div>
                }
            </div>
        </div>
    );
}

export default Kenowledge;

