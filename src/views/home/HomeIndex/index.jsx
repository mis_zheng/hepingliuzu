import React, { useEffect,useState } from "react";
import { useDispatch } from "react-redux";
import Swiper from "@/component/Swiper/Swiper";
import Right from "@/component/Right/Right";
import Rightbum from "@/component/Rightbum/Rightbum";
import Screen from "@/component/Screen/Screen";
import api from "@/api";
import useRequest from "@/hook/useRequest";
import '@/style/home.less';
function HomeIndex() {
  
    const dispatch = useDispatch();
   const [flag, setflag]=useState(true)
    const list =(useRequest(api.getTag)).list
    const lists =(useRequest(api.getRecommend)).list
    
    useEffect(() => {
          dispatch({
                type: 'getlist',
                payload: lists,
            });
        },[lists]);

  return (
    <div className="homeindex">
      <div className="homeindexs">
      <div className="homeindex-left">
            <Swiper list={lists} />
           <Screen setflag={setflag}></Screen>
      </div>
    
      <div className="homeindex_right">
      <Right  rlist={lists}></Right>
      <Rightbum list={list}></Rightbum>
      </div>
      </div>
    </div>
  );
}

export default HomeIndex;
