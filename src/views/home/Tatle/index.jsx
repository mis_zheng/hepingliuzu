import React,{useState,useEffect}from 'react';
import api from "../../../api/index";
import useRequest from "../../../hook/useRequest";
import {useLocation,useNavigate} from "react-router-dom";
import "../../../style/Tatle/tatle.less"
function Tatle(){
    const {list,error}=useRequest(api.getTag);
    const useloc=useLocation()
    const usenav=useNavigate()
    return(
        <div className='Tatle'>
            
            <div className='TatleLeft'>
                <div className='TatleTop'></div>
                <header className='header'>
                    <h1>yu<span>{useloc.state.label}</span>tagRelativeArticles</h1>
                </header>
                <main className='main'>
                <h4>tagTitle</h4>
                <ul className='mainUl'>
                    {
                        list&&list.map((item,index)=>{
                            return <li key={index} className={item.id==useloc.state.id?"action":''} onClick={()=>{
                                usenav("/home/Tatle",{state:item})
                            }}>{item.label}</li>
                        })
                    }
                </ul>
                </main>
                <footer className='footer'>
                    <h1>empty</h1>
                </footer>
            </div>
        </div>
    )
}

export default Tatle