import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import RouterViews from './router/RouterViews';
import store from './store';

import { Provider } from 'react-redux';
import "./i18n/confog"
const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
    <Provider store={store}>
        <RouterViews />
    </Provider>
);
