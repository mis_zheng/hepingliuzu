import { legacy_createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
const initialState = {
    list: [],
    daytime: true,
    swiperflag: false,
    searchlist: [],
    titlelist: [],
    articletitle: [],
    title: "",
    val: "en",
    category: []
};

const reducer = (
    state = initialState,
    { type, payload }
) => {
    const newState = JSON.parse(JSON.stringify(state));
    switch (type) {
        // 国际化
        case 'setval':
            newState.val = payload;
            return newState;
        case 'article':
            newState.articletitle = payload
            return {
                ...state,
                articletitle: payload[0]
            }
        case 'titlelist':
            newState.titlelist = payload
            return newState
        case "gettwolist":
            newState.brrlist = payload
            return {
                ...state,
                brrlist: payload[0]

            }
        case 'searchlist':
            newState.searchlist = state.list.filter(item => item.title.includes(payload))
            return newState
        case 'getlist':
            newState.list = payload;
            return newState;
        case 'gettitle':
            newState.title = payload;
            return newState;
        // 白天黑夜控制
        case 'setDaytime':
            newState.daytime = !payload;
            return newState;
        // 详情数据获取
        case 'getcategory':
            newState.category = payload;
            return newState;
        
        default:
            return newState;
    }
};

export default legacy_createStore(
    reducer,
    applyMiddleware(thunk, logger)
);

