'use strict';

const path = require('path');
const camelcase = require('camelcase');

// This is a custom Jest transformer turning file imports into filenames.
// http://facebook.github.io/jest/docs/en/webpack.html

module.exports = {
    process(src, filename) {
<<<<<<< HEAD
        const assetFilename = JSON.stringify(
            path.basename(filename)
        );
=======
<<<<<<< HEAD
        const assetFilename = JSON.stringify(
            path.basename(filename)
        );
=======
        const assetFilename = JSON.stringify(path.basename(filename));
>>>>>>> 0f3d79f2af011daeee68fc39626fdce95cfc39c4
>>>>>>> 6bd19b0176b3e0c86c249bbb7dbf9d7d3994fe55

        if (filename.match(/\.svg$/)) {
            // Based on how SVGR generates a component name:
            // https://github.com/smooth-code/svgr/blob/01b194cf967347d43d4cbe6b434404731b87cf27/packages/core/src/state.js#L6
<<<<<<< HEAD
            const pascalCaseFilename = camelcase(path.parse(filename).name, {
                pascalCase: true,
            });
=======
<<<<<<< HEAD
            const pascalCaseFilename = camelcase(
                path.parse(filename).name,
                {
                    pascalCase: true,
                }
            );
=======
            const pascalCaseFilename = camelcase(path.parse(filename).name, {
                pascalCase: true,
            });
>>>>>>> 0f3d79f2af011daeee68fc39626fdce95cfc39c4
>>>>>>> 6bd19b0176b3e0c86c249bbb7dbf9d7d3994fe55
            const componentName = `Svg${pascalCaseFilename}`;
            return `const React = require('react');
      module.exports = {
        __esModule: true,
        default: ${assetFilename},
        ReactComponent: React.forwardRef(function ${componentName}(props, ref) {
          return {
            $$typeof: Symbol.for('react.element'),
            type: 'svg',
            ref: ref,
            key: null,
            props: Object.assign({}, props, {
              children: ${assetFilename}
            })
          };
        }),
      };`;
        }

        return `module.exports = ${assetFilename};`;
    },
};
